
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `class_name` varchar(100) NOT NULL,
  PRIMARY KEY (`class_id`),
  KEY `school_id` (`school_id`),
  CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `school` (`school_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES (1,1,'Class B'),(2,1,'Class A'),(3,1,'Class C'),(4,2,'Food P'),(5,2,'Curl A'),(6,2,'WE&MF'),(7,3,'S1 A'),(8,4,'S3 B'),(9,4,'S2 A');
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotion` (
  `promotion_id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_period` varchar(70) NOT NULL,
  `promotion_nick_name` varchar(200) NOT NULL,
  PRIMARY KEY (`promotion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotion`
--

LOCK TABLES `promotion` WRITE;
/*!40000 ALTER TABLE `promotion` DISABLE KEYS */;
INSERT INTO `promotion` VALUES (1,'2016',''),(2,'2017',''),(5,'2018','Awesome promotion');
/*!40000 ALTER TABLE `promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relation_tbl`
--

DROP TABLE IF EXISTS `relation_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relation_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `promotion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `class_id` (`class_id`),
  CONSTRAINT `relation_tbl_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`),
  CONSTRAINT `relation_tbl_ibfk_2` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relation_tbl`
--

LOCK TABLES `relation_tbl` WRITE;
/*!40000 ALTER TABLE `relation_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `relation_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school`
--

DROP TABLE IF EXISTS `school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school` (
  `school_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(200) NOT NULL,
  `school_email` varchar(70) NOT NULL,
  `school_address` varchar(150) NOT NULL,
  `school_motto` varchar(200) NOT NULL,
  PRIMARY KEY (`school_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school`
--

LOCK TABLES `school` WRITE;
/*!40000 ALTER TABLE `school` DISABLE KEYS */;
INSERT INTO `school` VALUES (1,'RCA','rca.ac.rw','Western province,Nyabihu district Rwanda','Leading Through digital innovation'),(2,'N.TVET','nyabihutvet@gmail.com','Musanze-Rubavu road,Nyabihu district Rwanda','Promoting market orinted skills'),(3,'E.S.SE','essaintesprit@ac.rw','Huye-Kigali-road,southern province Rwanda','Science,culture and discipline with excellence'),(4,'CXR','cxr@gmail.com','Nyanza South',']');
/*!40000 ALTER TABLE `school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_names` varchar(100) NOT NULL,
  `student_email` varchar(70) NOT NULL,
  `student_address` varchar(200) NOT NULL,
  `class_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  PRIMARY KEY (`student_id`),
  KEY `class_id` (`class_id`),
  KEY `school_id` (`school_id`),
  KEY `promotion_id` (`promotion_id`),
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`),
  CONSTRAINT `student_ibfk_2` FOREIGN KEY (`school_id`) REFERENCES `school` (`school_id`),
  CONSTRAINT `student_ibfk_3` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`promotion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'Twizeyimana jacques','sandberg@gmail.com','Nyanza South',1,1,1),(2,'Munyakazi Isaac','munyakazi.ac.rw','',3,2,1),(4,'Manzi mike','manzimike@gmail.com','Rwanda Kigali',4,2,2),(5,'nsimiye emmy','nsim.com','nyabihuru',5,2,2),(6,'Munyawera','munyawera@gmail.com','Nyagatare Rwanda',7,3,5),(7,'Ndeshyo brave','ndeshyo@gmail.com','',6,2,2),(8,'Manzi Mike','manzimike378@gmail.com','',8,4,5),(9,'Munyakazi Isaac','isaacMunya@yahoo.fr','Nyanza Rwanda',9,4,5);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;
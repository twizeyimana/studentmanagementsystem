#include</usr/include/mysql/my_global.h>

#include</usr/include/mysql/mysql.h>

#include<string.h>

#include<stdlib.h>

char temp[1];
void validate(MYSQL * connection) {
    fprintf(stderr, "error:%s\n", mysql_error(connection));
    exit(0);
}

MYSQL * connectToDB() {
    MYSQL * initialConnection = mysql_init(NULL);
    if (
        mysql_real_connect(initialConnection, "127.0.0.1", "db_username", "user_password", "schoolManagementSystem", 3306, NULL, 0) == NULL)
        validate(initialConnection);
    return initialConnection;
}

void getSchoolId() {
    MYSQL * connection = connectToDB();
    int query_result = mysql_query(connection, "select school_id,school_name from school");
    if (query_result)
        validate(connection);

    MYSQL_RES * result = mysql_store_result(connection);
    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    printf("\n\t\t\t                LIST OF SCHOOLS  AVILABLE                    \n");
    printf("\t\t\t_______________________________________________________________\n");
    printf("\t\t\t\tId\t\t\t\tSchool Name\n");


    while (row = mysql_fetch_row(result)) {
        printf("\n");
        for (int i = 0; i < num_fields; i++) {

            printf("\t\t\t\t%s", row[i]);
        }
        printf("\n");
    }
    printf("\t\t\t______________________________________________________________\n");

}
void getclassId(char * schoolId) {
    MYSQL * connection = connectToDB();
    char query[80] = "select class_id,class_name from class where school_id = ";
    strcat(query, schoolId);
    int query_result = mysql_query(connection, query);
    if (query_result)
        validate(connection);

    MYSQL_RES * result = mysql_store_result(connection);
    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    printf("\t\t\t            LIST OF CLASSES AVAILABLE IN THIS SCHOOL           \n");
    printf("\t\t\t_______________________________________________________________\n");

    printf("\t\t\t\tclass id\t\tclass name\n");
    while (row = mysql_fetch_row(result)) {
        printf("\n");
        for (int i = 0; i < num_fields; i++) {

            printf("\t\t\t\t%s", row[i]);
        }
        printf("\n");
    }
    printf("\t\t\t______________________________________________________________\n");

}

void get_academic_years() {
    MYSQL * connection = connectToDB();
    char query[80] = "select promotion_id,promotion_period from promotion";
    int query_result = mysql_query(connection, query);
    if (query_result)
        validate(connection);

    MYSQL_RES * result = mysql_store_result(connection);
    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    printf("\t\t\t            LIST OF ACADEMIC YEARS AVAILABLE IN THIS SCHOOL           \n");
    printf("\t\t\t_______________________________________________________________\n");

    printf("\t\t\t\tACADEMIC ID\t\t ACADEMIC PERIOD\n");
    while (row = mysql_fetch_row(result)) {
        printf("\n");
        for (int i = 0; i < num_fields; i++) {

            printf("\t\t\t\t%s", row[i]);
        }
        printf("\n");
    }
    printf("\t\t\t______________________________________________________________\n");

}

void schoolRegistration() {
    MYSQL * connection = connectToDB();
    char school_name[200], school_email[70], school_address[150], school_motto[100];
    printf("\n\t\t------------------ REGISTERING NEW SCHOOL ----------------------- \n\n");
    printf("\t\t----------------- Enter school name -------------------- \n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", school_name);

    printf("\t\t----------------- Enter school email ------------------- \n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", school_email);

    printf("\t\t----------------- Enter school address ----------------- \n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", school_address);
    printf("\t\t----------------- Enter school motto ------------------- \n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", school_motto);
    char query[2000] = "INSERT INTO school(school_name,school_email,school_address,school_motto) VALUES(";
    strcat(query, "'");
    strcat(query, school_name);
    strcat(query, "'");
    strcat(query, ",");
    strcat(query, "'");
    strcat(query, school_email);
    strcat(query, "'");
    strcat(query, ",");
    strcat(query, "'");
    strcat(query, school_address);
    strcat(query, "'");
    strcat(query, ",");
    strcat(query, "'");
    strcat(query, school_motto);
    strcat(query, "')");

    //printf("%s\n", query);
    int insertSchool = mysql_query(connection, query);
    if (insertSchool)
        validate(connection);
    printf("\n\t\t      SCHOOL REGISTERED SUCCESSFULY \n\n");
}
void classRegistration() {
    MYSQL * connection = connectToDB();
    char school_id[5], class_name[20];

    getSchoolId();
    printf("\t\tEnter the school_id your class is located in\n\t\t");
    scanf("%s", school_id);

    printf("\t\t -------Enter class name-----------------------------------------\n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", class_name);

    char query[200] = "INSERT INTO class(school_id,class_name) values(";
    strcat(query, school_id);
    strcat(query, ",");
    strcat(query, "'");
    strcat(query, class_name);
    strcat(query, "')");

    //printf("Query used :%s\n", query);
    int createClass = mysql_query(connection, query);
    if (createClass)
        validate(connection);
    printf("\t\t      CLASS REGISTERED SUCCESSFULY \t\t\n");
}

void studentRegistration() {
    MYSQL * connection = connectToDB();
    char school_id[5], class_id[5], student_names[100], student_email[70], student_address[200], promotion[20];
    getSchoolId();
    printf("\t\tEnter school_id to which this student will belong to \n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", school_id);

    getclassId(school_id);
    printf("\t\tEnter class_id to which this student will belong to \n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", class_id);
    printf("\t\tEnter student full names\n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", student_names);

    printf("\t\tEnter student email\n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", student_email);

    printf("\t\tEnter student address \n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", student_address);
    get_academic_years();
    printf("\t\tEnter the academic year_id which this student will attend \n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", promotion);

    char query[500] = "insert into student(class_id,school_id,promotion_id,student_names,student_email,student_address) values('";
    strcat(query, class_id);
    strcat(query, "'");
    strcat(query, ",");
    strcat(query, "'");
    strcat(query, school_id);
    strcat(query, "'");
    strcat(query, ",");
    strcat(query, "'");
    strcat(query, promotion);
    strcat(query, "'");
    strcat(query, ",");
    strcat(query, "'");
    strcat(query, student_names);
    strcat(query, "'");
    strcat(query, ",");

    strcat(query, "'");
    strcat(query, student_email);
    strcat(query, "'");
    strcat(query, ",");

    strcat(query, "'");
    strcat(query, student_address);
    strcat(query, "')");
    //printf("%s", query);
    int registerStudent = mysql_query(connection, query);
    if (registerStudent)
        validate(connection);
    printf("\n\t\t      STUDENT REGISTERED SUCCESSFULY \n\n");
}

void academic_registration() {
    char peroid[20], comments[200], query[300] = "INSERT INTO promotion(promotion_period,promotion_nick_name) VALUES ('";
    printf("\t\t  ACADEMIC YEAR REGISTRATION \t\t\n");
    printf("\t\t Enter Academic year peroid example :2019\n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", peroid);
    printf("\t\t Add academic year comments or nickname if you would like\n\t\t");
    scanf("%c", temp);
    scanf("%[^\n]s", comments);
    strcat(query, peroid);
    strcat(query, "'");
    strcat(query, ",");
    strcat(query, "'");
    strcat(query, comments);
    strcat(query, "')");

    MYSQL * connection = connectToDB();
    int insertYear = mysql_query(connection, query);
    if (insertYear) {
        printf("%s", query);
        validate(connection);
    } else
        printf("\n\t\t  ACADEMIC YEAR WAS REGISTERED SUCCESSFULY \n");
}
void get_totals(char * id) {
    MYSQL * connection = connectToDB();

    int promo_query = mysql_query(connection, "select * from promotion");
    MYSQL_RES * result__ = mysql_store_result(connection);
    MYSQL_ROW row__;
    while (row__ = mysql_fetch_row(result__)) {
        //now we have one promotion
        char get_std_nums_query[300] = "select count(*) from student inner join class on student.class_id = class.class_id inner join school on class.school_id = school.school_id  where school.school_id = ";
        strcat(get_std_nums_query, id);
        strcat(get_std_nums_query, "  and student.promotion_id =  ");
        strcat(get_std_nums_query, row__[0]);
        int get_std_ = mysql_query(connection, get_std_nums_query);
        MYSQL_RES * store_result = mysql_store_result(connection);
        MYSQL_ROW std_total;
        while (std_total = mysql_fetch_row(store_result)) {
            printf("%s\t\t", std_total[0]);
        }
    }
    char get_total_for_school[200] = "select count(*) from student inner join class on student.class_id = class.class_id inner join school on class.school_id = school.school_id  where school.school_id = ";
    strcat(get_total_for_school, id);
    mysql_query(connection, get_total_for_school);
    MYSQL_RES * tt_skul = mysql_store_result(connection);
    MYSQL_ROW ttl;
    while (ttl = mysql_fetch_row(tt_skul)) {
        //printf("%s",get_total_for_school);
        printf("%s", ttl[0]);
    }
    printf("\n");
}

void print_last_total() {
    MYSQL * connection = connectToDB();
    printf("\nTotal students by year:\t\t");
    mysql_query(connection, "select * from promotion");
    MYSQL_RES * total_result = mysql_store_result(connection);
    MYSQL_ROW total_result_row;
    while (total_result_row = mysql_fetch_row(total_result)) {
        char select_std_by_ac[150] = "select count(*) from student where promotion_id ='";

        strcat(select_std_by_ac, total_result_row[0]);
        strcat(select_std_by_ac, "'");

        mysql_query(connection, select_std_by_ac);
        MYSQL_RES * num = mysql_store_result(connection);
        MYSQL_ROW std_by_ac_row;

        while (std_by_ac_row = mysql_fetch_row(num)) {
            printf("%s\t\t", std_by_ac_row[0]);
        }
    }
    mysql_query(connection, "select count(*) from student");
    MYSQL_RES * abanyeshuri_bose_hamwe_res = mysql_store_result(connection);
    MYSQL_ROW abanyeshuri_bose_hamwe_row;
    while (abanyeshuri_bose_hamwe_row = mysql_fetch_row(abanyeshuri_bose_hamwe_res)) {
        printf("%s\n", abanyeshuri_bose_hamwe_row[0]);
    }
    printf("_____________________________________________________________________________________________\n");
}

void readData() {
    printf("\n  schools\tClasses\t\t");
    MYSQL * connection = connectToDB();
    int years_query = mysql_query(connection, "select * from promotion");
    MYSQL_RES * result_ = mysql_store_result(connection);
    // int num_col = mysql_num_fields(result_);
    MYSQL_ROW row_;
    while (row_ = mysql_fetch_row(result_)) {
        printf("%s\t\t", row_[1]);
    }
    printf("Total\n");

    printf("___________________________________________________________________________________________\n");

    int school_query = mysql_query(connection, "select * from school");
    MYSQL_RES * result_slip = mysql_store_result(connection);
    int num_cols = mysql_num_fields(result_slip);
    MYSQL_ROW row;

    while (row = mysql_fetch_row(result_slip)) {
        //print school
        printf("   %s\n\t\t", row[1]);

        char classquery[200] = "select * from class where school_id = '";
        strcat(classquery, row[0]);
        strcat(classquery, "'");
        int class_query = mysql_query(connection, classquery);

        MYSQL_RES * result_class = mysql_store_result(connection);
        int class_cols = mysql_num_fields(result_class);
        MYSQL_ROW class_row;

        //print classes
        while (class_row = mysql_fetch_row(result_class)) {
            printf("%s\t\t", class_row[2]);

            int promo_query = mysql_query(connection, "select * from promotion");
            MYSQL_RES * result__ = mysql_store_result(connection);
            MYSQL_ROW row__;
            while (row__ = mysql_fetch_row(result__)) {

                //get number of students in class in this academic year
                char get_students_query[200] = "select count(*) from student where ";
                strcat(get_students_query, "class_id = ");
                strcat(get_students_query, class_row[0]);
                strcat(get_students_query, " AND promotion_id = ");
                strcat(get_students_query, row__[0]);

                int needed_students = mysql_query(connection, get_students_query);
                MYSQL_RES * result_2 = mysql_store_result(connection);
                int class_cols = mysql_num_fields(result_2);
                MYSQL_ROW std_row;

                //print the number of students
                while (std_row = mysql_fetch_row(result_2)) {
                    printf("%s\t\t", std_row[0]);
                }
            }

            char get_students_in_class[80] = "select count(*) from student where class_id = ";
            strcat(get_students_in_class, class_row[0]);
            int total_std = mysql_query(connection, get_students_in_class);
            MYSQL_RES * class_std_res = mysql_store_result(connection);
            MYSQL_ROW class_std_row;

            while (class_std_row = mysql_fetch_row(class_std_res)) {
                printf("%s", class_std_row[0]);
            }

            printf("\n\t\t");

        }
        printf("\n Totals\t\t\t\t");
        get_totals(row[0]);
        printf("_____________________________________________________________________________________________\n");
    }
    print_last_total();
}
#include<stdio.h>
#include "functions.c"

//gcc -o msql  main.c -I/usr/include/mysql $(mysql_config --libs)

int main() {
    int option;
    do {
        printf("\n\t\t||------------------------------------------------------------------------------||\n");
        printf("\t\t||                                                                              ||\n");
        printf("\t\t||                                                                              ||\n");
        printf("\t\t||        WELCOME TO SANDBERG STUDENTS  MANAGEMENT  SYSTEM                      ||\n");
        printf("\t\t||                                                                              ||\n");
        printf("\t\t||                                                                              ||\n");
        printf("\t\t||------------------------------------------------------------------------------||\n");
        printf("\t\t||                                                                              ||\n");
        printf("\t\t||          1.INSERT THE SCHOOLS                                                ||\n");
        printf("\t\t||          2.INSERT THE CLASS IN THE SCHOOL                                    ||\n");
        printf("\t\t||          3.INSERT THE STUDENT IN THE CLASS                                   ||\n");
        printf("\t\t||          4.REGISTER NEW ACADEMIC YEAR                                        ||\n");
        printf("\t\t||          5.VIEW THE REPORT FORM                                              ||\n");
        printf("\t\t||          6.EXIT                                                              ||\n");
        printf("\t\t||                                                                              ||\n");
        printf("\t\t||------------------------------------------------------------------------------||\n");
        printf("\t\t||          ENTER NUMBER FOR THE SERVICE YOU WANT \t\t");
        scanf("%d", & option);

        switch (option) {
            case 1:
                schoolRegistration();
                break;
            case 2:
                classRegistration();
                break;
            case 3:
                studentRegistration();
                break;
            case 4:
                academic_registration();
                break;

            case 5:
                system("clear");
                readData();
                break;
            case 6:
                exit(1);
                break;

            default:
                printf("\n\t\tDear system user,enter valid choice from given ones \n\n");
                break;
        }
    } while (option != 6);
    return 0;
}